import os
import json
from typing import Union, List, Dict


class ExperiencesHelper:
    @staticmethod
    def get_all_files_recursively(
        dir_path: str, array_of_files: List[str] = []
    ) -> List[str]:
        """Recursively retrieves all files in a directory.

        Args:
            dir_path (str): The path to the directory.
            array_of_files (List[str]): List to store file paths.

        Returns:
            List[str]: List of file paths.
        """
        for root, _, files in os.walk(dir_path):
            for file in files:
                array_of_files.append(os.path.join(root, file))
        return array_of_files

    @staticmethod
    def read_domain_fixture_json(
        experience_identifier, experiences_base_path="out/experiences"
    ) -> Union[Dict[str, Union[str, bool]], None]:
        """Reads a JSON fixture file based on an experience identifier.

        Args:
            experience_identifier: Identifier to match in file paths.
            experiences_base_path (str): Base path for experiences directory.

        Returns:
            Union[Dict[str, Union[str, bool]], None]: Parsed JSON content.
        Raises:
            ValueError: If no matching files are found or JSON decoding error occurs.
        """
        list_of_file_path = ExperiencesHelper.get_all_files_recursively(
            f"{experiences_base_path}/tuiexperiences"
        )
        file_path = next(
            (
                experiences_file_path
                for experiences_file_path in list_of_file_path
                if experience_identifier in experiences_file_path
            ),
            None,
        )

        if not file_path:
            raise ValueError(
                f"No files match the filter: {experience_identifier}"
            )
        else:
            resolved_path = os.path.abspath(file_path)
            with open(resolved_path, "r") as file_content:
                try:
                    return json.load(file_content)
                except json.JSONDecodeError as e:
                    raise ValueError(f"{e.msg} in {file_content}")

    @staticmethod
    def generate_experience_page_url(
        base_url,
        experience_identifier,
        city,
        country_code="",
        experiences_base_path="out/experiences",
    ) -> str:
        """Generates a URL for an experience page.

        Args:
            base_url (str): Base URL for the website.
            experience_identifier: Identifier to match in file paths.
            city (str): City name.
            country_code (str): Country code (optional).
            experiences_base_path (str): Base path for experiences directory.

        Returns:
            str: Generated URL.
        """
        experience_slug = ExperiencesHelper.read_domain_fixture_json(
            experience_identifier,
            experiences_base_path=experiences_base_path,
        )["slug_id"]
        if country_code:
            return f"{base_url}/{country_code}/{city}/{experience_slug}"
        else:
            return f"{base_url}/{city}/{experience_slug}"

    @staticmethod
    def get_experience_title(
        experience_identifier, experiences_base_path="out/experiences"
    ) -> str:
        """Gets the title of an experience based on its identifier.

        Args:
            experience_identifier: Identifier to match in file paths.
            experiences_base_path (str): Base path for experiences directory.

        Returns:
            str: Experience title.
        """
        return ExperiencesHelper.read_domain_fixture_json(
            experience_identifier=experience_identifier,
            experiences_base_path=experiences_base_path,
        )["title"]

    @staticmethod
    def get_promo_code_value(
        experience_identifier, experiences_base_path="out/experiences"
    ) -> str:
        """Gets the promo code value of an experience based on its identifier.

        Args:
            experience_identifier: Identifier to match in file paths.
            experiences_base_path (str): Base path for experiences directory.

        Returns:
            str: Promo code value.
        """
        return ExperiencesHelper.read_domain_fixture_json(
            experience_identifier=experience_identifier,
            experiences_base_path=experiences_base_path,
        )["code"]
