.DEFAULT_GOAL:=help
CUR_DIR:=$$(pwd)
SHELL ?= /bin/zsh
export PIPENV_VENV_IN_PROJECT=true

help:
	@echo "❓ helps section"
	@grep -E '^[a-zA-Z_-]+.*?## .*$$' Makefile | sed 's/:.*##/##/' | sort | awk 'BEGIN {FS = "## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

activate-virtual-env: ## 🔌   Activate Python virtual environment
	@echo "🔌 activating python virtual environment"
	@if [ -z "$$VIRTUAL_ENV" ]; then \
		pipenv shell; \
	else \
		echo "Virtual environment is already activated."; \
	fi

echo-virtual-env: ## 🗨️    Echo Python virtual environment
	@echo "🐍 Python virtual environment present in $(VIRTUAL_ENV)"

format: ## 🧰   Apply code formatting changes
	@echo "🔧 Running Black to auto-format Python code..."
	pipenv run black .

format-check: ## 👁️‍🗨️    Check possible code formatting changes (dry-run)
	@echo "🔧 Running Black auto-format Python code check (dry-run)..."
	pipenv run black --check . || true

format-check-ci: ## 👁️‍🗨️    Check possible code formatting changes in CI/CD
	@echo "🔧 Running Black auto-format Python code check in CI/CD..."
	pipenv run black --check .

format-diff: ## 🔄   Preview code formatting changes (dry-run)
	@echo "🔧 Running Black auto-format Python code preview (diff)..."
	pipenv run black --diff .	|| true

install-pre-commit-hook: ## 🪝   Install Git pre-commit hook
	@echo "🔌 Installing Git pre-commit hook..."
	pre-commit install

install-virtual-env: ## 🛠️    Install Python virtual environment
	@echo "🌐 Installing python virtual environment"
	pipenv install --dev

install-virtual-env-ci: ## 🛠️    Install Python virtual environment in CI/CD
	@echo "🌐 Installing python virtual environment in CI/CD"
	pip install pipenv
	pipenv install --deploy --dev

lint: ## 🚀   Perform lint check with Flake8
	@echo "🔍 Running Flake8 to check code style and quality...."
	pipenv run flake8 . || true

lint-ci: ## 🚀   Perform lint check with Flake8 in CI/CD
	@echo "🔍 Running Flake8 to check code style and quality in CI/CD...."
	pipenv run flake8

prepare-package: ## 📦 Create distribution package
	@echo "🔨 Creating distribution package...."
	@if [ -d build ]; then rm -r build; fi
	@if [ -d dist ]; then rm -r dist; fi
	pipenv run python -m build

pre-commit-all: ## 🚦   Run pre-commit hooks against the codebase
	@echo "🏁 Running pre-commit hooks on the whole codebase"
	pre-commit run --all-files --verbose

remove-pre-commit-hook: ## 🚫   Remove Git pre-commit hook
	@echo "🧹 Removing Git pre-commit hook..."
	rm -rvf .git/hooks/pre-commit

remove-virtual-env: ## 🗑️🛠   Remove Python virtual environment
	@echo "🌐 removing python virtual environment"
	-pipenv --rm 2>/dev/null || true
	(unset PIPENV_VENV_IN_PROJECT; \
    unset VIRTUAL_ENV; \
    unset VIRTUAL_ENV_PROMPT; \
    echo "Environment variables unset in this shell")

run-tests: ## ✅🧪 Running tests
	@echo "✅🧪 Running tests"
	pipenv run pytest --verbose

semantic-release-dry-run:
	PYTHONPATH=. pipenv run semantic-release --noop -vv version

semantic-release:
	PYTHONPATH=. pipenv run semantic-release --verbose version

semantic-release-strict-dry-run:
	 PYTHONPATH=. pipenv run semantic-release --noop -vv  --strict version

semantic-release-strict:
	 PYTHONPATH=. pipenv run semantic-release --verbose --strict version

show-packages-graph: ## 📊   Show Python packages graph
	@echo "📈 showing Python packages graph"
	pipenv graph

typecheck: ## 🧐 Run type checking
	@echo "🔍 Running mypy to analyze code for potential type issues..."
	pipenv run mypy --pretty . || true

typecheck-ci: ## 🧐 Run type checking in CI/CD
	@echo "🔍 Running mypy to analyze code for potential type issues in CI/CD...."
	pipenv run mypy --pretty .

upload-package: ## 🚀 Upload distribution package to Gitlab registry
	@echo "📤 Uploading to Gitlab registry...."
	TWINE_USERNAME=gitlab-ci-token TWINE_PASSWORD=${RELEASE_TOKEN} pipenv run python -m twine upload --verbose --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
