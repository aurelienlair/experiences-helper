# Python TUI experiences helper

The scope of this project is to learn how to build and deploy homemade Python packages

## Installation

This chapter guides you through the installation process, ensuring that you have all the necessary tools and dependencies to run the project.

### Set Up a virtual environment

Establish the virtual environment and install Python requirements. The virtual environment will be automatically installed within a directory named `.venv` by default.

```bash
make install-virtual-env
```

And for activating the virtual environment

```bash
make activate-virtual-env
```

Getting Started

If necessary, you can still uninstall it by executing

```bash
make remove-virtual-env
```

### Running unit tests

The following instructions will allow you to set the project up from scratch.

```bash
make run-tests
```

### Build and deploy the python package

To build and deploy against Gitlab registry just run the following commands.

```bash
make semantic-release-strict
export RELEASE_TOKEN=super-secret-token 
export CI_API_V4_URL=https://gitlab.com/api/v4
export CI_PROJECT_ID=55319175
make upload-package
```

## Code Quality and Formatting Tools

### Flake8

[Flake8](https://flake8.pycqa.org/en/latest/) serves as a Python linting tool designed to assess Python code for compliance with coding standards and to pinpoint potential errors and issues related to code quality. When applied, Flake8 evaluates Python code against multiple coding standards and guidelines, encompassing [PEP 8](https://peps.python.org/pep-0008/). It detects various concerns such as syntax errors, undefined variables, unused imports, code complexity, and more. Detailed explanations of [error codes](https://pycodestyle.pycqa.org/en/latest/intro.html#error-codes) can be referenced for further understanding.

Execute it in the local environment by running:

```bash
make lint
```

ℹ️ It is automatically executed in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline.

### Black

[Black](https://black.readthedocs.io/en/stable/index.html) is a Python code formatter designed to automatically adjust Python code, ensuring adherence to a consistent and opinionated coding style.

Unlike linting tools, Black is solely focused on code formatting. It imposes a specific set of rules and conventions related to code layout, indentation, line length, and other formatting aspects.

For additional information, refer to the Black command line [options](https://black.readthedocs.io/en/stable/usage_and_configuration/the_basics.html#command-line-options).

Black is compatible with configurations in various formats for common tools. In this project, I have integrated it with Flake8 and Pre-commit. For more details, please refer to the following pages:

- [Black with Flake8](https://black.readthedocs.io/en/stable/guides/using_black_with_other_tools.html#flake8)
- [Black with Pre-commit](https://black.readthedocs.io/en/stable/integrations/source_version_control.html)

To verify if there are some files to be formatted (dry-run) execute:

```bash
make format-check
```

To preview the formatting changes that will be applied to the code, execute the following:

```bash
make format-diff
```

Execute to apply the code formatting changes:

```bash
make format
```

ℹ️ Black is automatically executed in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline.

### Pre-commit

[Pre-commit](https://pre-commit.com/#intro) proves valuable in identifying straightforward issues before code undergoes review. By performing checks and actions prior to committing code, it aids in early issue detection during development, diminishing the chance of bugs and ensuring adherence to project-specific coding standards. If you wish to learn more about the installation process, refer to the [following](https://pre-commit.com/#3-install-the-git-hook-scripts) page.

To install pre-commit during the initial setup, execute:

```bash
make install-pre-commit-hook
```

Henceforth, if you attempt to commit a file with formatting errors or non-compliance with Python [PEP standards](https://peps.python.org/pep-0008/), the commit will be unsuccessful.

### Mypy

In this project, I am using [Mypy](https://mypy-lang.org/) an optional [static type checker](https://en.wikipedia.org/wiki/Type_system#Type_checking) for Python. It seeks to integrate the advantages of dynamic (or "duck") typing with static typing.

Run the following command to perform static code analysis with Mypy on the codebase:

```bash
make typecheck
```

ℹ️ Unlike Flake8 or Black, Mypy intentionally hasn't been included as a pre-commit hook. This decision stems from the fact that when Mypy attempts to type-check code before committing, in a nutshell it uses a virtual environment. I observed that running Mypy on a virtual environment not strictly identical to the one used in the current project resulted in numerous errors or false positives, prompting me to deliberately discard the idea. Nonetheless, Mypy is automatically run in the [CI/CD](https://en.wikipedia.org/wiki/CI/CD) pipeline.

## Git commit message convention

This projects follows this convention:

```shell
 <jira-ticket> | <type>: <subject>
```

Example:

```shell
TICKET-123 | docs: add description of a make command
```

| Type         | Description                                                                                            |
| ------------ | ------------------------------------------------------------------------------------------------------ |
| `style`    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build`    | Changes to the build process                                                                           |
| `chore`    | Changes to the build process or auxiliary tools and libraries such as documentation generation         |
| `docs`     | Documentation updates                                                                                  |
| `feat`     | New features                                                                                           |
| `fix`      | Bug fixes                                                                                              |
| `refactor` | Code refactoring                                                                                       |
| `test`     | Adding missing tests                                                                                   |
| `perf`     | A code change that improves performance                                                                |

## Useful links

- [Flake8](https://flake8.pycqa.org/en/latest/)
- [Black](https://black.readthedocs.io/en/stable/index.html)
- [Pre-commit](https://pre-commit.com/#intro)
- [Mypy](https://mypy-lang.org/)
- [Python Semantic Release](https://python-semantic-release.readthedocs.io/en/stable/index.html)
