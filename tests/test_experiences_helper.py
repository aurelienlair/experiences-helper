import pytest
from experiences_helper import ExperiencesHelper


class TestExperiencesHelper:
    BASE_URL = "https://www.tuiexperiences.com"
    EXPERIENCE_IDENTIFIER = "ticket-to-orsay-museum-with-dedicated-entrance"
    EXPERIENCE_FIXTURE_NAME = f"{EXPERIENCE_IDENTIFIER}.json"
    EXPERIENCES_DIRECTORY = "./tests/out/experiences"
    EXPERIENCE_FILE_PATH = f"{EXPERIENCES_DIRECTORY}/tuiexperiences/{EXPERIENCE_IDENTIFIER}/{EXPERIENCE_FIXTURE_NAME}"
    EXPERIENCE_SLUG_NAME = (
        "ticket-to-orsay-museum-with-dedicated-entrance-32543"
    )
    CITY = "paris"
    COUNTRY_CODE = "us"

    @pytest.mark.unit
    def test_get_all_files_recursively_should_return_a_correct_list(
        self,
    ) -> None:
        returned_value = ExperiencesHelper.get_all_files_recursively(
            dir_path=f"{self.EXPERIENCES_DIRECTORY}/tuiexperiences"
        )
        assert isinstance(returned_value, list)
        assert returned_value == [self.EXPERIENCE_FILE_PATH]

    @pytest.mark.unit
    def test_generate_experience_page_url_without_country_code(self) -> None:
        received = ExperiencesHelper.generate_experience_page_url(
            base_url=self.BASE_URL,
            experience_identifier=self.EXPERIENCE_IDENTIFIER,
            city=self.CITY,
            experiences_base_path=self.EXPERIENCES_DIRECTORY,
        )
        expected = f"{self.BASE_URL}/{self.CITY}/{self.EXPERIENCE_SLUG_NAME}"

        assert received == expected

    @pytest.mark.unit
    def test_generate_experience_page_url_with_country_code(self) -> None:
        received = ExperiencesHelper.generate_experience_page_url(
            base_url=self.BASE_URL,
            experience_identifier=self.EXPERIENCE_IDENTIFIER,
            city=self.CITY,
            country_code=self.COUNTRY_CODE,
            experiences_base_path=self.EXPERIENCES_DIRECTORY,
        )
        expected = f"{self.BASE_URL}/{self.COUNTRY_CODE}/{self.CITY}/{self.EXPERIENCE_SLUG_NAME}"

        assert received == expected

    @pytest.mark.unit
    def test_read_domain_fixture_json(self) -> None:
        received = ExperiencesHelper.read_domain_fixture_json(
            experience_identifier=self.EXPERIENCE_IDENTIFIER,
            experiences_base_path=self.EXPERIENCES_DIRECTORY,
        )
        expected = {
            "name": "ticket-to-orsay-museum-with-dedicated-entrance-01",
            "key": "experience_uuid",
            "value": "b7b0ed7a-b108-424a-a79c-dcebb547bbc2",
            "enabled": True,
            "slug_id": "ticket-to-orsay-museum-with-dedicated-entrance-32543",
            "title": "ticket-to-orsay-museum-with-dedicated-entrance-01 | Ticket to Orsay Museum with dedicated entrance",
            "code": "A_303N10",
        }
        assert received == expected

    @pytest.mark.unit
    def test_error_on_non_existing_experience_identifier(self) -> None:
        non_existing_experience = "non-existing-experience-identifier"

        expected_error_message = (
            f"No files match the filter: {non_existing_experience}"
        )

        with pytest.raises(ValueError, match=expected_error_message):
            ExperiencesHelper.read_domain_fixture_json(
                experience_identifier=non_existing_experience,
                experiences_base_path=self.EXPERIENCES_DIRECTORY,
            )

    @pytest.mark.unit
    def test_should_return_experience_title(self) -> None:
        received = ExperiencesHelper.get_experience_title(
            experience_identifier=self.EXPERIENCE_IDENTIFIER,
            experiences_base_path=self.EXPERIENCES_DIRECTORY,
        )
        expected = "ticket-to-orsay-museum-with-dedicated-entrance-01 | Ticket to Orsay Museum with dedicated entrance"
        assert received == expected

    @pytest.mark.unit
    def test_should_return_promo_code(self):
        received = ExperiencesHelper.get_promo_code_value(
            experience_identifier=self.EXPERIENCE_IDENTIFIER,
            experiences_base_path=self.EXPERIENCES_DIRECTORY,
        )
        expected = "A_303N10"
        assert received == expected
